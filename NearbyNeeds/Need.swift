//
//  Need.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import Foundation

class Need {
  var name:String?
  var desc:String?
  
  init(name:String, desc:String) {
    self.name = name
    self.desc = desc
  }
}
