//
//  Model.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import Foundation

class Model {
  static let sharedInstance = Model()
  var user:User?
  var currentLocations:Dictionary<String,Location>?
  var currentLocation:String?
  var selectedSubneed:String = "Toilets"
}
