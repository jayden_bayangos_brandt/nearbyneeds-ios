//
//  LoginVC.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import SwiftCop

class LoginVC: UIViewController {
  
  @IBOutlet weak var fldEmail: UITextField!
  @IBOutlet weak var fldPass: UITextField!
  
  lazy var prefs = UserDefaults.standard
  let model = Model.sharedInstance
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    doesUserExist()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func onLogin(_ sender: AnyObject) {
    if validateFields() {
      login(email: fldEmail.text!, pass: fldPass.text!)
    }
  }
  
  func login(email:String, pass:String) {
    FIRAuth.auth()?.signIn(withEmail: email, password: pass, completion: {
      (user, error) in
      if let error = error {
        print(error.localizedDescription)
      } else {
        self.saveCredentials(uid: user!.uid, email: email, pass: pass)
        self.getUserDetails(uid: user!.uid)
        self.loginSuccess()
      }
    })
  }
  
  func getUserDetails(uid:String) {
    let ref = FIRDatabase.database().reference()
    ref.child("Users").child(uid).observeSingleEvent(of: .value, with: { snapshot in
      let value = snapshot.value as? [String:AnyObject]
      let favourites = value?["favourites"] as? [String:AnyObject]
      var faves = Dictionary<String, SubNeed>()
      for child in favourites! {
        let name = child.value["name"] as? String
        let desc = child.value["description"] as? String
        let fave = SubNeed(name: name!, desc: desc!)
        faves[name!] = fave
      }
      self.model.user?.faves = faves
      
    })
  }
  
  func loginSuccess() {
    self.performSegue(withIdentifier: "MainVC", sender: nil)
  }
  
  func saveCredentials(uid:String, email:String, pass:String) {
    // Save to user defaults
    var credentials = Dictionary<String, String>()
    credentials["Email"] = email
    credentials["Password"] = pass
    credentials["Uid"] = uid
    prefs.set(credentials, forKey: "Credentials")
    // Save to model
    var user = User()
    user.email = email
    user.pass = pass
    user.uid = uid
    model.user = user
  }
  
  func doesUserExist() {
    if let creds = prefs.dictionary(forKey: "Credentials") {
      let email = creds["Email"] as? String
      let pass = creds["Password"] as? String
      login(email: email!, pass: pass!)
    } else {
      return
    }
    
  }
  
  func showAlert(title:String, message:String) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let dismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
    alertController.addAction(dismiss)
    self.present(alertController, animated: true, completion: nil)
  }
  
  func validateFields() -> Bool{
    var issue = false
    let emailTrial = Trial.email
    var trial = emailTrial.trial()
    if !trial(fldEmail.text!) {
      issue = true
    }
    let lengthTrial = Trial.length(.minimum, 2)
    trial = lengthTrial.trial()
    if !trial(fldPass.text!) {
      issue = true
    }
    if issue {
      showAlert(title: "Error", message: "Email or password invalid")
      return false
    } else {
      return true
    }
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
