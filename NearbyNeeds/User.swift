//
//  User.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct User {
  var name:String?
  var pass:String?
  var uid:String?
  var email:String?
  var rep:Int?
  var faves:Dictionary<String, SubNeed> = Dictionary<String, SubNeed>()
  
  mutating func addFave(subNeed:SubNeed) {
    faves[subNeed.name!] = subNeed
    updateFirebase()
  }
  
  mutating func removeFave(subNeed:SubNeed) {
    faves.removeValue(forKey: subNeed.name!)
    updateFirebase()
  }
  
  func updateFirebase() {
    let ref = FIRDatabase.database().reference()
    ref.child(uid!).child("favourites").setValue(convert())
  }
  
  func convert() -> Dictionary<String, AnyObject> {
    var objects = Dictionary<String, AnyObject>()
    for need in faves.values {
      var object = Dictionary<String, AnyObject>()
      object["name"] = need.name! as AnyObject?
      object["description"] = need.desc! as AnyObject?
      object["subNeed"] = need.isSubNeed as AnyObject?
      
      objects[need.name!] = object as AnyObject?
    }
    return objects
  }
  

  
}
