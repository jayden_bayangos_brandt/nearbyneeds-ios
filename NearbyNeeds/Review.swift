//
//  Review.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import Foundation

struct Review {
  var exists:Bool?
  var comments:String?
  var name:String?
  var date:String?
  
  init(exists:Bool, comments:String, name:String, date:String) {
    self.exists = exists
    self.comments = comments
    self.name = name
    self.date = date
  }
}
