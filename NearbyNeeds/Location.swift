//
//  Location.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import Foundation

struct Location {
  var name:String?
  var coords:Dictionary<String, String> = Dictionary<String, String>()
  var rep:Int?
  var reviews:Dictionary<String, Review> = Dictionary<String, Review>()
  var need:String?
  var image:String?
  var uid:String?
}
