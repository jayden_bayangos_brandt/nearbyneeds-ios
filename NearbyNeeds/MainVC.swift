//
//  ViewController.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import UIKit
import GoogleMaps
import FirebaseDatabase
import FirebaseAuth
import NotificationCenter
import CoreLocation

class MainVC: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
  
  @IBOutlet weak var mapView: GMSMapView!
  
  var ref:FIRDatabaseReference!
  let model = Model.sharedInstance
  var btnSearch:UIBarButtonItem!
  var btnNewLocation:UIBarButtonItem!
  var locManager:CLLocationManager!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    ref = FIRDatabase.database().reference()
    setupSearchBar()
    setupLocationManager()
    getLocations(need: model.selectedSubneed)
    // Update locations on subneed change
    NotificationCenter.default.addObserver(self, selector: #selector((self.updateLocations)), name:NSNotification.Name(rawValue: "SubNeedChanged"), object: nil)
  }
  
  func setupLocationManager() {
    locManager = CLLocationManager()
    locManager.delegate = self
    locManager.requestWhenInUseAuthorization()
    locManager.desiredAccuracy = kCLLocationAccuracyKilometer
    locManager.requestLocation()
  }
  
  // Setup maps when coords available
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = manager.location
    setupMaps(lat: (location?.coordinate.latitude)!, long: (location?.coordinate.longitude)!)
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("Error: \(error.localizedDescription)")
  }
  
  func updateLocations() {
    getLocations(need: model.selectedSubneed)
  }
  
  func showSearchDialog() {
    self.performSegue(withIdentifier: "SearchSegue", sender: nil)
    print("Search")
  }
  
  // Go to create location view
  func createLocation() {
    self.performSegue(withIdentifier: "NewLocationSegue", sender: nil)
    print("New Location")
  }
  
  func setupSearchBar() {
    self.btnSearch = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.search, target: self, action: #selector(showSearchDialog))
    self.btnNewLocation = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self, action: #selector(createLocation))
    self.navigationItem.rightBarButtonItems = [btnNewLocation, btnSearch]
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // Go to location detail
  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    let name = marker.title
    print(title)
    model.currentLocation = name
    self.performSegue(withIdentifier: "ReviewSegue", sender: nil)
    return true
  }
  
  func setupMaps(lat:Double, long:Double) {
    mapView.delegate = self
    mapView.isMyLocationEnabled = true
    mapView.settings.myLocationButton = true
    let coords = CLLocationCoordinate2D(latitude: lat, longitude: long)
    mapView.isMyLocationEnabled = true
    mapView.animate(toLocation: coords)
    mapView.animate(toZoom: 8)
  }
  
  func getLocations(need:String) {
    ref.child(need).observeSingleEvent(of: .value, with: {
      (snapshot) in
      var locations = Dictionary<String, Location>()
      let value = snapshot.value as? [String:AnyObject]
      for child in value! {
        var location = Location()
        if let name = child.value["name"] as? String {
          location.name = name
        }
        if let coords = child.value["coords"] as? [String:AnyObject] {
          let lat = coords["Latitude"] as? String
          let long = coords["Longitude"] as? String
          location.coords["Latitude"] = lat
          location.coords["Longitude"] = long
        }
        if let image = child.value["image"] as? String {
          location.image = image
        }
        if let need = child.value["need"] as? String {
          location.need = need
        }
        if let rep = child.value["reputation"] as? Int {
          location.rep = rep
        }
        if let uid = child.value["uid"] as? String {
          location.uid = uid
        }
        // Add to dictionary
        locations[location.name!] = location
      }
      // Add to map
      let array = Array(locations.values)
      self.refreshMarkers(locations: array)
      self.model.currentLocations = locations
      
    }) { (error) in
      print("Error with markers: \(error.localizedDescription)")
    }
  }
  
  func refreshMarkers(locations:[Location]) {
    mapView.clear()
    for location in locations {
      let lat = Double(location.coords["Latitude"]!)
      let long = Double(location.coords["Longitude"]!)
      let position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
      let marker = GMSMarker(position: position)
      marker.title = location.name
      marker.icon = GMSMarker.markerImage(with: UIColor.blue)
      marker.map = mapView
    }
  }
  
  
  
}

