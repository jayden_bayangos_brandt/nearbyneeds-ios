//
//  NewLocationVC.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 4/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import UIKit
import MultiAutoCompleteTextSwift
import FirebaseDatabase
import CoreLocation
import GoogleMaps
import FirebaseStorage

class NewLocationVC: UIViewController, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
  @IBOutlet weak var lblCoords: UILabel!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var fldTitle: UITextField!
  @IBOutlet weak var fldAutoComplete: MultiAutoCompleteTextField!
  
  var ref:FIRDatabaseReference!
  var needArray = [String]()
  var locManager:CLLocationManager!
  var imageURL:String!
  var location:Dictionary<String, String>!
  let model = Model.sharedInstance
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    ref = FIRDatabase.database().reference()
    loadingIndicator.isHidden = true
    getSubNeeds()
    fldAutoComplete.text = model.selectedSubneed
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func getPicture(_ sender: AnyObject) {
    let imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    imagePicker.sourceType = .photoLibrary
    present(imagePicker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    picker.dismiss(animated: true, completion: nil)
    imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
  }
  @IBAction func onGrabLocation(_ sender: AnyObject) {
    setupLocationManager()
  }
  
  @IBAction func onSubmit(_ sender: AnyObject) {
    if checkFields() {
      uploadToFirebase()
    }
  }
  
  func uploadImage(key:String) {
    let image = imageView.image
    let data:Data = UIImagePNGRepresentation(image!)!
    let storage = FIRStorage.storage()
    let storageRef = storage.reference(forURL: "gs://firebase-nearbyneeds.appspot.com")
    let imagesRef = storageRef.child("locations/\(key).jpeg")
    let uploadTask = imagesRef.put(data, metadata: nil) { metadata, error in
      if error != nil {
        print(error?.localizedDescription)
      } else {
        print(metadata?.downloadURL()?.absoluteString)
        self.dismiss(animated: true, completion: nil)
      }
    }
  }
  
  func uploadToFirebase() {
    var location = Location()
    location.name = fldTitle.text!
    location.need = fldAutoComplete.text!
    location.image = imageURL
    location.coords = self.location
    
    ref = FIRDatabase.database().reference()
    let key = ref.child(location.need!).childByAutoId().key
    let imageString = UUID().uuidString
    
    var object = Dictionary<String, AnyObject>()
    object["name"] = location.name! as AnyObject?
    object["need"] = location.need! as AnyObject?
    object["image"] = imageString as AnyObject?
    object["reputation"] = 0 as AnyObject?
    object["uid"] = key as AnyObject?
    object["coords"] = self.location as AnyObject

    ref = FIRDatabase.database().reference()
    ref.child(location.need!).child(key).setValue(object)
    uploadImage(key: imageString)
  }
  
  func setupLocationManager() {
    self.loadingIndicator.startAnimating()
    self.loadingIndicator.isHidden = false
    locManager = CLLocationManager()
    locManager.delegate = self
    locManager.desiredAccuracy = kCLLocationAccuracyBest
    locManager.requestLocation()
  }
  
  // Gets location and reverse geocodes to get street address
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let coords = locations.first
    let geocoder = GMSGeocoder()
    geocoder.reverseGeocodeCoordinate((coords?.coordinate)!, completionHandler: { response, error in
      if let address = response?.firstResult() {
        let lines = address.lines! as [String]
        // Set coords label to address
        self.lblCoords.text = lines.joined(separator: "\n")
        // Save coords to model
        var dict = Dictionary<String,String>()
        dict["Latitude"] = String(describing: coords?.coordinate.latitude)
        dict["Longitude"] = String(describing: coords?.coordinate.longitude)
        self.location = dict
        // Hide indicator
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true
      } else {
        print(error?.localizedDescription)
        self.loadingIndicator.stopAnimating()
        self.loadingIndicator.isHidden = true
      }
      })
    
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print(error)
  }
  
  func getSubNeeds() {
    ref.child("SubNeedsList").observeSingleEvent(of: .value, with: { snapshot in
      let value = snapshot.value as? [String:AnyObject]
      for child in value! {
        let name = child.value["name"] as? String
        self.needArray.append(name!)
        self.fldAutoComplete.autoCompleteStrings = self.needArray
      }
    })
  }
  
  func checkFields() -> Bool {
    if fldTitle.text == "" {
      showAlert()
      return false
    }
    if !compareSubNeed(subneed: fldAutoComplete.text!) {
      showAlert()
      return false
    }
    
    if lblCoords.text == "Coords" {
      showAlert()
      return false
    }
    
    return true
  }
  
  // Check if user query for subNeed exists
  func compareSubNeed(subneed:String) -> Bool {
    for name in needArray {
      if needArray.contains(name) {
        return true
      }
    }
    return false
  }
  
  func showAlert() {
    let alert = UIAlertController(title: "Error", message: "Missing fields", preferredStyle: .alert)
    let dismiss = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
    alert.addAction(dismiss)
    self.present(alert, animated: true, completion: nil)
  }
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
