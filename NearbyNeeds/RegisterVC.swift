//
//  RegisterVC.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import UIKit
import SwiftCop
import FirebaseAuth

class RegisterVC: UIViewController {

  @IBOutlet weak var fldEmail: UITextField!
  @IBOutlet weak var fldPass: UITextField!
  @IBOutlet weak var fldPassTwo: UITextField!
  
  let model = Model.sharedInstance
  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  @IBAction func onRegister(_ sender: AnyObject) {
    if validateFields() {
      createUser()
    }
  }
  
  func createUser() {
    FIRAuth.auth()?.createUser(withEmail: fldEmail.text!, password: fldPass.text!, completion: { user, error in
      if error != nil {
        print(error?.localizedDescription)
        self.showAlert(title: "Error", message: (error?.localizedDescription)!)
      } else {
        self.saveCredentials(uid: (user?.uid)!, email: self.fldEmail.text!, pass: self.fldPass.text!)
        self.finishRegister()
      }
    })
  }
  
  func finishRegister() {
    self.performSegue(withIdentifier: "MainVC", sender: nil)
  }
  
  func saveCredentials(uid:String, email:String, pass:String) {
    let prefs = UserDefaults.standard
    var credentials = Dictionary<String, String>()
    credentials["Email"] = email
    credentials["Password"] = pass
    credentials["Uid"] = uid
    prefs.set(credentials, forKey: "Credentials")
    
    var user = User()
    user.email = email
    user.pass = pass
    user.uid = uid
    model.user = user
  }
  
  func validateFields() -> Bool{
    var issue = false
    let emailTrial = Trial.email
    var trial = emailTrial.trial()
    if !trial(fldEmail.text!) {
      issue = true
    }
    let lengthTrial = Trial.length(.minimum, 2)
    trial = lengthTrial.trial()
    if !trial(fldPass.text!) {
      issue = true
    }
    if fldPass.text! != fldPassTwo.text! {
      issue = false
    }
    if issue {
      showAlert(title: "Error", message: "Email or password invalid")
      return false
    } else {
      return true
    }
  }
  
  func showAlert(title:String, message:String) {
  let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
  let dismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
  alertController.addAction(dismiss)
  self.present(alertController, animated: true, completion: nil)
  }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
