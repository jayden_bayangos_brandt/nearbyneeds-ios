//
//  SearchVC.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 3/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SearchVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
  
  @IBOutlet weak var tableview: UITableView!
  @IBOutlet weak var fldSearch: UISearchBar!
  var results = [SubNeed]()
  var ref:FIRDatabaseReference!
  let model = Model.sharedInstance
  override func viewDidLoad() {
    super.viewDidLoad()
    ref = FIRDatabase.database().reference()
    view.backgroundColor = UIColor.clear
    view.isOpaque = false
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func onDismiss(_ sender: AnyObject) {
    self.dismiss(animated: true, completion: nil)
  }
  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return results.count
  }
  
  @IBAction func onSwitch(_ sender: AnyObject) {
    let swt = sender as? UISwitch
    let index = swt?.tag
    let subNeed = results[index!]
    if (swt?.isOn)! {
      // Add favourite
      model.user?.addFave(subNeed: subNeed)
      print("Add")
    } else {
      // Remove favourite
      model.user?.removeFave(subNeed: subNeed)
      print("Remove")
    }
  }
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    // Do query for subNeeds
    let query = ref.child("SubNeedsList").queryOrdered(byChild: "name").queryStarting(atValue: searchBar.text!)
    query.observe(FIRDataEventType.value, with: { snapshot in
      let dict = snapshot.value as! [String:AnyObject]
      for child in dict {
        let name = child.value["name"] as? String
        let desc = child.value["description"] as? String
        let subNeed = child.value["subNeed"] as? String
        let sub = SubNeed(name: name!, desc: desc!)
        if subNeed == nil {
          sub.isSubNeed = true
        }
        switch (name!) {
        case "Toilets":
          break
        case "Bike racks":
          break
        case "Drinking fountains":
          break
        case "Rubbish bins":
          break
        default:
          self.results.append(sub)
          self.tableview.reloadData()
          
        }
      }
    })
  }
  
  
  // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
  // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
  
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? SearchTableViewCell
    cell?.lblTitle.text = results[indexPath.row].name
    cell?.swtFave.isOn = isFave(name: results[indexPath.row].name!)
    cell?.swtFave.tag = indexPath.row
    
    return cell!
  }
  
  func isFave(name:String) -> Bool {
    return model.user?.faves[name] != nil
  }
  
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
