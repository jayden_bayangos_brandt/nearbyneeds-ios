//
//  SearchTableViewCell.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 3/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {

  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var swtFave: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
