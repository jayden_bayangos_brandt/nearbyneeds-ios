//
//  ReviewVC.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 3/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import UIKit
import Cosmos

class ReviewVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet weak var imgView: UIImageView!
  @IBOutlet weak var lblTitle: UILabel!
  @IBOutlet weak var lblDesc: UILabel!
  @IBOutlet weak var rating: CosmosView!
  @IBOutlet weak var tableview: UITableView!
  
  var location:Location?
  let model = Model.sharedInstance
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    setupLocation()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func addReview(_ sender: AnyObject) {
  }
  
  func setupLocation() {
    location = model.currentLocations?[model.currentLocation!]
    lblTitle.text = location?.name!
    rating.rating = Double((location?.rep!)!)
  }
  
  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return (location?.reviews.count)!
  }
  
  // Needs further implementation
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    cell.textLabel?.text = "Title"
    cell.detailTextLabel?.text = "Desc"
    return cell
  }
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
