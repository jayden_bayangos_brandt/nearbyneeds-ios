//
//  DrawerTableVC.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 3/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import UIKit
import SideMenu
import NotificationCenter

class DrawerTableVC: UITableViewController {
  
  let model = Model.sharedInstance
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = false
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    tableView.sectionHeaderHeight = 50
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 2
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch (section) {
      // Defaults subNeeds
    case 0:
      return 4
      // User subNeeds
    case 1:
      return (model.user?.faves.count)!
    default:
      return 0
    }
  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    switch (section) {
    case 0:
      return "DefaultNeeds"
    case 1:
      return "SubNeeds"
    default:
      return "Error"
    }
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    // Setup rows for default subneeds
    var cell:UITableViewCell
    if (indexPath.section == 0) {
      switch (indexPath.row) {
      case 0:
        cell = tableView.dequeueReusableCell(withIdentifier: "Toilets", for: indexPath)
        cell.textLabel?.text = "Toilets"
        return cell
      case 1:
        cell = tableView.dequeueReusableCell(withIdentifier: "Bike", for: indexPath)
        cell.textLabel?.text = "Bike racks"
        return cell
      case 2:
        cell = tableView.dequeueReusableCell(withIdentifier: "Drinking", for: indexPath)
        cell.textLabel?.text = "Drinking Fountains"
        return cell
      case 3:
        cell = tableView.dequeueReusableCell(withIdentifier: "Rubbish", for: indexPath)
        cell.textLabel?.text = "Rubbish bins"
        return cell
      default:
        cell = tableView.dequeueReusableCell(withIdentifier: "Dynamic", for: indexPath)
        cell.textLabel?.text = "Dynamic 2"
        return cell
      }
    } else {
      // Setup rows for user custom subNeeds
      cell = tableView.dequeueReusableCell(withIdentifier: "Dynamic", for: indexPath)
      let name = getLocationName(row: indexPath.row)
      cell.textLabel?.text = name
      return cell
    }
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.section == 0 {
      let name = tableView.cellForRow(at: indexPath)!.textLabel!.text!
      model.selectedSubneed = name
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SubNeedChanged"), object: nil)
      self.dismiss(animated: true, completion: nil)
    }
  }
  
  func getLocationName(row:Int) -> String {
    return Array(model.user!.faves.values)[row].name!
  }
  
  
  /*
   // Override to support conditional editing of the table view.
   override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
   // Return false if you do not want the specified item to be editable.
   return true
   }
   */
  
  /*
   // Override to support editing the table view.
   override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
   if editingStyle == .delete {
   // Delete the row from the data source
   tableView.deleteRows(at: [indexPath], with: .fade)
   } else if editingStyle == .insert {
   // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
   }
   }
   */
  
  /*
   // Override to support rearranging the table view.
   override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
   
   }
   */
  
  /*
   // Override to support conditional rearranging of the table view.
   override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
   // Return false if you do not want the item to be re-orderable.
   return true
   }
   */
  
  /*
   // MARK: - Navigation
   
   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */
  
}
