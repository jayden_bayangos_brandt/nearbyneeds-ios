//
//  SubNeed.swift
//  NearbyNeeds
//
//  Created by Jayden Bayangos-Brandt on 2/11/16.
//  Copyright © 2016 JBayangosB. All rights reserved.
//

import Foundation

class SubNeed:Need {
  var createdBy:String?
  var isSubNeed = true
  
  init(name:String, desc:String, createdBy:String, isSubNeed:Bool) {
    super.init(name: name, desc: desc)
    self.createdBy = createdBy
    self.isSubNeed = isSubNeed
  }
  
  override init(name:String, desc:String) {
    super.init(name: name, desc: desc)
  }
}
